# Face identification web app

A web application that identifies the user by face and after successfully authorization allows signing in to reach the sensitive data.

## Description
The application contains the Registration template, through which the new user creates a new account by inputting email,
name and password and then by learning the ML model of getting the embeddings of user's face via PC's camera.
<br />
For 20 seconds model learns the new user's face and stores all the input frames taken with user's face in ../src/face_identifier/dataset_processing/images/@email/. 
The labeled data of user's email and embeddings (transformed from taken images) is stored in ../src/face_identifier/dataset_processing/new_embeddings_with_labels.p.
<br />
The labeled embeddings are created thanks to [faced](https://github.com/iitzco/faced) and [face_recognition](https://github.com/ageitgey/face_recognition) packages.
<br />
When the new user is added to the database and the ML model is updated by the new user's face embeddings, the user is redirected to the Sign in template.
<br />
After inputting the right email and password and confirming the sign-in process, the camera takes a couple of frames to identify the user's face.
Then the taken frames are transformed into embeddings and ML model (../src/face_identifier/model.p) predicts to which embeddings collected in the new_embeddings_with_labels.p is the user's face the most similar to.
The verification of this prediction allows the user to sign-in or not.
<br />
One of the main advantages of this face authorization solution is that it doesn't require much computational power to add a new user and update the ML model.
<br />
The finally selected ML model is [KNeighborsClassifier](https://scikit-learn.org/stable/modules/generated/sklearn.neighbors.KNeighborsClassifier.html) 
which uses as an input user's face embeddings in form of List of 128 numbers with labels and therefore GPU usage is not necessary.

## Registration template:

![](Matylda.gif)
<br />
## Login template:

![](Matylda2.gif)

## Installation
To run the web app on your local host: a
1. Open your Python IDE program.
2. Create the venv. The project by default was created in [Poetry](https://python-poetry.org/).
3. Clone the repository.
```
git clone https://gitlab.com/Zedbog/web_application_that_identifies_the_user_by_face.git
```
4. Install the required packages from requirements.txt or use the poetry.lock.
5. Go to file ../src/web_app/database/create_database.py and run it to create the db.sqlite database.
6. Run main.py
7. Enter the local host.
8. Do not forget to allow yours PC's camera run. 

## Usage
This web application can be used as a prologue for further development of any web application which will require the sophisticated authorization to identify the user who tries to reach the sensitive data. The aim of this application is just to demonstrate the possibility of usage of face authorization concept which may be used in any application.

## Support
In case of any need please do not hesitate to contact me directly via [mat.konkel@gmail.com](mailto:mat.konkel@gmail.com).

## Authors and acknowledgment
Many thanks to 
1. [Ivan Itzcovich](https://github.com/iitzco) for [faced](https://github.com/iitzco/faced) Real Time face detection package.
2. [Adam Geitgey](https://github.com/ageitgey) for [face_recognition](https://github.com/ageitgey/face_recognition) package, which allowed getting the embeddings of user's faces.
