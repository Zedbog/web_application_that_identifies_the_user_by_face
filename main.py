from flask import Blueprint, render_template
from flask_login import current_user, login_required
from web_application_that_identifies_the_user_by_face.src.web_app import create_app

main = Blueprint("main", __name__)


@main.route("/")
def index():
    return render_template("index.html")


@main.route("/profile")
@login_required
def profile():
    return render_template("profile.html", name=current_user.name)


if __name__ == "__main__":
    app = create_app()
    app.run()
