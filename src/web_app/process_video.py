import os
import cv2
from flask import flash, redirect, url_for
from pathlib import Path
from web_application_that_identifies_the_user_by_face.src.face_identifier.model import Model


def show_webcam():
    camera = cv2.VideoCapture(0)
    while True:
        success, frame = camera.read()  # read the camera frame
        if not success:
            flash("Błąd! Brak kamery!")
            return redirect(url_for("auth.login"))
        else:

            ret, buffer = cv2.imencode(".jpg", frame)
            frame = buffer.tobytes()

            yield (
                b"--frame\r\n" b"Content-Type: image/jpeg\r\n\r\n" + frame + b"\r\n"
            )  # concat frame one by one and show result


def compare_photo(email):
    camera = cv2.VideoCapture(0)
    frames = []
    frame_number = 0
    success, frame = camera.read()
    # Due to often problem that camera couldn't start successfully
    # -> create a loop in which camera is being forced to run successfully
    # -> loop ends when camera is successfully opened
    while not success:
        camera.release()
        camera = cv2.VideoCapture(0)
        success, frame = camera.read()
    while True:
        success, frame = camera.read()  # read the camera frame
        if not success:
            flash("Błąd! Brak kamery!")
            return redirect(url_for("auth.login"))
        if frame_number % 2 == 0:
            frames.append(frame)
        elif frame_number > 100:
            break
        frame_number += 1

    model = Model()
    verification_result = model.verify(frames, email)
    return verification_result


def do_snapshots(email):
    camera = cv2.VideoCapture(0)
    frame_number = 0

    while True:
        frame_number += 1
        success, frame = camera.read()  # read the camera frame
        path = "src/face_identifier/dataset_processing/images"
        Path(os.path.join(path, email)).mkdir(parents=True, exist_ok=True)

        if not success:
            flash("Błąd! Brak kamery!")
            return redirect(url_for("auth.signup"))
        else:
            if frame_number % 2 == 0:
                cv2.imwrite(
                    os.path.join(path, email + "/" + email + "_" + str(frame_number) + ".jpg"), frame
                )
                print(os.path.join(path, email + "/" + email + "_" + str(frame_number) + ".jpg"))
            ret, buffer = cv2.imencode(".jpg", frame)
            frame = buffer.tobytes()

            yield (
                b"--frame\r\n" b"Content-Type: image/jpeg\r\n\r\n" + frame + b"\r\n"
            )  # concat frame one by one and show result
