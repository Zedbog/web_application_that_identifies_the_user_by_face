from flask_login import UserMixin

from . import db
from web_application_that_identifies_the_user_by_face.src.web_app import create_app

class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(100))
    name = db.Column(db.String(1000))
