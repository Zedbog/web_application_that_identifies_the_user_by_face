from web_application_that_identifies_the_user_by_face.src.web_app import db, create_app


"""Script to create the database instance."""
db.create_all(app=create_app())
