"""Module enables to detect and processed faces."""
from typing import List, Tuple
import cv2
import face_recognition
import numpy as np
from faced import FaceDetector
from faced.utils import annotate_image


class FaceFinder:
    """FaceFinder class."""

    def __init__(self, threshold: float = 0.85):
        """
        Class for finding face.

        :param threshold: probability threshold
        :type threshold: float
        """
        self.threshold = threshold
        self.face_detector = FaceDetector()

    def get_bounding_boxes_from_image(
        self, img: np.ndarray, to_rgb: bool = True, most_probable: bool = True
    ) -> List[Tuple[int, int, int, int, float]]:
        """
        Bounding boxes getter for face detection.

        :param img: processed image/frame
        :type img: np.ndarray
        :param to_rgb: if image is in BGR format (default format)
        :type to_rgb: bool
        :param most_probable: return only most probable bounding box (if many)
        :type most_probable: bool
        :return: list of bounding boxes found on image in
        (x_center, y_center, width, height, probability) format
        :rtype: List[Tuple[int,int,int,int,float]]
        """
        if to_rgb:
            img = cv2.cvtColor(img.copy(), cv2.COLOR_BGR2RGB)

        bounding_boxes = self.face_detector.predict(img, thresh=self.threshold)
        bounding_boxes.sort(key=lambda x: x[-1], reverse=True)

        if most_probable and len(bounding_boxes) > 1:
            bounding_boxes = [bounding_boxes[0]]
        return bounding_boxes

    def detect_face_and_draw_on_image(self, img: np.ndarray) -> None:
        """
        Detect all faces and draw them on one single image.

        :param img: processed image/frame
        :type img: np.ndarray
        :return: None
        :rtype: None
        """
        bounding_boxes = self.get_bounding_boxes_from_image(img=img)
        ann_img = annotate_image(img, bounding_boxes)

        cv2.imshow("Image", ann_img)
        cv2.waitKey(1)

    def detect_faces_and_draw_on_video(self) -> None:
        """
        Detect most probable face and draw it on video.

        :return: None
        :rtype: None
        """
        video_capture = cv2.VideoCapture(0)

        while True:
            _, frame = video_capture.read()

            bounding_boxes = self.get_bounding_boxes_from_image(
                img=frame, most_probable=False
            )
            ann_img = annotate_image(frame, bounding_boxes)

            cv2.imshow("Video", ann_img)

            # Hit 'q' on the keyboard to quit!
            if cv2.waitKey(1) & 0xFF == ord("q"):
                break

        # Release handle to the webcam
        video_capture.release()
        cv2.destroyAllWindows()

    def get_embeddings(self, img: np.ndarray, precision: int = 1) -> List[float]:
        """
        Get embeddings function.

        :param img: processed image/frame
        :type img: np.ndarray
        :param precision: specify precision of encodings
        (!!! 10 = 10x longer computation)
        :type precision: int
        :return: List of 128 numbers - representation of the face.
        :rtype: List[float]
        """
        bounding_box = self.get_bounding_boxes_from_image(img=img)
        encodings = face_recognition.face_encodings(
            img, known_face_locations=bounding_box, num_jitters=precision, model="large"
        )
        if encodings:
            return encodings[0]
        else:
            return []
