from model import Model
import dataset_processing


model = Model()

fd = dataset_processing.FaceDataset(data_storage="../face_identifier/dataset_processing/new_embeddings_with_labels.p")
fd.load_data()
new_embeddings = [
    embedding
    for embedding, label in zip(fd.embeddings, fd.labels)
    if label != "Unknown_Person"
]
new_labels = [
    label
    for embedding, label in zip(fd.embeddings, fd.labels)
    if label != "Unknown_Person"
]

unknown = 0
for embedding, label in zip(fd.embeddings, fd.labels):
    if label == "Unknown_Person":
        unknown += 1
        new_embeddings.append(embedding)
        new_labels.append(label)
        if unknown > 400:
            break
print(f'Set of people: {set(new_labels)}')
print('Start training model..')
print('Display confusion Matrix')
model.model_training(new_embeddings, new_labels)
print('Comparison of different models')
model.model_comparison(new_embeddings, new_labels)
print('Best params for KNN:')
model.search4BestParams(new_embeddings, new_labels)
