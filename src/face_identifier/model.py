import pickle
import numpy as np
from collections import Counter
from typing import List
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.ensemble import AdaBoostClassifier, RandomForestClassifier
from sklearn.model_selection import cross_val_score, train_test_split, GridSearchCV
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import confusion_matrix

from web_application_that_identifies_the_user_by_face.src.face_finder import FaceFinder


class Model:
    """Model class."""

    def __init__(self, model_storage: str = "../web_application_that_identifies_the_user_by_face/src/face_identifier/model.p") -> None:
        """
        Class for model train and use.

        :param model_storage: file where trained model will be stored
        :type model_storage: str
        """

        # self.classifier = AdaBoostClassifier()
        self.classifier = KNeighborsClassifier(n_neighbors=2, algorithm="auto", weights="distance", p=1)
        self.model_storage = model_storage

    @staticmethod
    def model_comparison(embeddings: List[List[float]], labels: List[str]) -> None:
        """
        Compare available machine learning face_identifier in same tasks using cross-validation.

        :param embeddings: List of face embeddings - features for training
        :type embeddings: List[List[float]]
        :param labels: List of labels (identifiers of persons)
        :type labels: List[str]
        :return: None
        :rtype: None
        """
        classifiers = [
            KNeighborsClassifier(n_neighbors=3, algorithm="ball_tree", weights="distance"),
            SVC(kernel="linear", C=0.025),
            SVC(gamma=2, C=1),
            DecisionTreeClassifier(max_depth=5),
            RandomForestClassifier(max_depth=5, n_estimators=10, max_features=1),
            MLPClassifier(alpha=1, max_iter=1000),
            AdaBoostClassifier(),
            GaussianNB(),
            QuadraticDiscriminantAnalysis(),
        ]
        for clf in classifiers:
            print(clf)
            print(
                cross_val_score(
                    clf, X=embeddings, y=labels, cv=5, scoring="f1_weighted"
                )
            )

    @staticmethod
    def search4BestParams(embeddings: List[List[float]], labels: List[str]) -> None:
        """
        Search for Best params of given classifier

        :param embeddings: List of face embeddings - features for training
        :type embeddings: List[List[float]]
        :param labels: List of labels (identifiers of persons)
        :type labels: List[str]
        :return: None
        :rtype: None
        """
        x_train, x_test, y_train, y_test = train_test_split(
            embeddings, labels, test_size=0.2, random_state=42, stratify=labels
        )

        params_knn = {
            'n_neighbors': [2, 3, 5, 7, 10, 15],
            'algorithm': ['auto', 'ball_tree', 'kd_tree'],
            'weights': ['uniform', 'distance'],
            'p': [1, 2]
        }

        clf = GridSearchCV(KNeighborsClassifier(), params_knn, scoring='f1_weighted')
        clf.fit(x_train, y_train)
        print(clf.best_params_)

    def model_training(
        self, embeddings: List[List[float]], labels: List[str]
    ) -> KNeighborsClassifier:
        """
        Trains model with given data.

        :param embeddings: List of face embeddings - features for training
        :type embeddings: List[List[float]]
        :param labels: List of labels (identifiers of persons)
        :type labels: List[str]
        :return: None
        :rtype: None
        """
        x_train, x_test, y_train, y_test = train_test_split(
            embeddings, labels, test_size=0.2, random_state=42, stratify=labels
        )
        self.classifier.fit(x_train, y_train)
        y_pred = self.classifier.predict(x_test)
        print(confusion_matrix(y_test, y_pred))
        return self.classifier

    def save_model(self) -> None:
        """
        Save model to specified model_storage file.

        :return: None
        :rtype: None
        """
        pickle.dump(self.classifier, open(self.model_storage, "wb"))

    def load_model(self) -> None:
        """
        Load model from specified model_storage file.

        :return: None
        :rtype: None
        """
        self.classifier = pickle.load(open(self.model_storage, "rb"))

    def verify(self, frames: List[np.ndarray], email: str) -> bool:
        """
        Verify specified person (by email).

        :param frames: person photos
        :type frames: List[np.ndarray]
        :param email: person identifier
        :type email: str
        :return: Is the right person or not?
        :rtype: bool
        """
        self.load_model()
        ff = FaceFinder()
        predictions = []
        for frame in frames:
            embeddings = ff.get_embeddings(frame)
            if len(embeddings) == 128:
                embeddings = np.array(embeddings)
                embeddings = embeddings.reshape(1, -1)
                predictions.append(self.classifier.predict(embeddings)[0])

        most_common = Counter(predictions).most_common(1)
        if email in most_common[0][0]:
            return True
        else:
            return False
