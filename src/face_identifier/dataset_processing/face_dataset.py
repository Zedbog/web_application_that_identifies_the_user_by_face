from typing import List
from web_application_that_identifies_the_user_by_face.src import face_finder
import os
import tqdm
import cv2
import pickle


class FaceDataset:
    """FaceDataset class."""

    def __init__(
        self,
        data_storage: str = "../web_application_that_identifies_the_user_by_face/src/face_identifier/dataset_processing/new_embeddings_with_labels.p",
        dataset_folder: str = "../dataset/lfw",
        one_person: bool = False,
    ) -> None:
        """
        Class for process and store dataset.

        :param data_storage: file where processed dataset will be stored
        :type data_storage: str
        :param dataset_folder: images data source
        :type dataset_folder: str
        """
        self.labels: List[str] = []
        self.embeddings: List[List[float]] = []
        self.data_storage = data_storage
        self.dataset_folder = dataset_folder
        self.one_person = one_person

    def data_processing(self, min_images_per_person: int = 200) -> None:
        """
        Get embeddings for all images in dataset.

        :param min_images_per_person: minimal number
        of photos that allows to be recognise in system
        :type min_images_per_person: int
        :return: None
        :rtype: None
        """
        ff = face_finder.FaceFinder()

        persons = os.listdir(self.dataset_folder)
        for person in tqdm.tqdm(persons):
            if self.one_person:
                photos = persons
                path = self.dataset_folder + "/"
            else:
                photos = os.listdir(self.dataset_folder + "/" + person)
                path = self.dataset_folder + "/" + person + "/"

            for photo in photos:
                img = cv2.imread(path + photo)
                embeddings = ff.get_embeddings(img)
                if len(embeddings) == 128:
                    if len(photos) > min_images_per_person:
                        self.labels.append(person)
                    else:
                        self.labels.append("Unknown_Person")
                    self.embeddings.append(embeddings)
            if self.one_person:
                break

    def save_data(self) -> None:
        """
        Save processed data to data_storage.

        :return: None
        :rtype: None
        """
        pickle.dump((self.embeddings, self.labels), open(self.data_storage, "wb"))

    def load_data(self) -> None:
        """
        Load processed data from data_storage.

        :return: None
        :rtype: None
        """
        self.embeddings, self.labels = pickle.load(open(self.data_storage, "rb"))

    def add_user_data_to_dataset(self) -> None:
        """
        Add new user processed data to dataset.

        :return: None
        :rtype: None
        """
        self.load_data()
        current_embeddings, current_labels = self.embeddings, self.labels
        self.data_processing()
        self.embeddings.extend(current_embeddings)
        self.labels.extend(current_labels)
        self.save_data()
